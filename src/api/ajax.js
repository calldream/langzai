// 这里模仿后台逻辑，处理数据库的东西
// 现用数据格式为： [{id:Number, [String]: any}]
// 理想数据格式：
// {[id:Number]: {show: Boolean, value: {[id: number], [key]: any}}}

export function setForm(name, value) {
    window.localStorage.setItem(name, JSON.stringify(value))
}
export function getForm(name) {
    const value = JSON.parse(window.localStorage.getItem(name) || '[]')
    value.last = () => {
        if (value.length) {
            return value[value.length - 1]
        } else {
            return null
        }
    }
    return value
}
export function getFormPage(name, query, page, size) {
    let allValue = getForm(name)
    for (let k in query) {
        if (query[k]) {
            allValue = allValue.filter(i => i[k].includes(query[k]))
        }
    }
    return {
        total: allValue.length,
        list: allValue.splice((page - 1) * size, size)
    }
}
export function addForm(name, value) {
    const old = getForm(name);
    const last = old.last()
    let k = 0
    if (last) {
        k = last.id / 1 + 1
    }
    value.id = k
    old.push(value)
    setForm(name, old)
}
export function deleForm(id, name) {
    const list = getForm(name)
    let index = list.findIndex(i => i.id === id)
    list.splice(index, 1)
    setForm(name, list)
}
export function editForm(item, name) {
    let list = getForm(name)
    let index = list.findIndex(i => i.id === item.id)
    list[index] = item
    setForm(name, list)
}
