import { getForm, getFormPage, addForm, deleForm, editForm, setForm } from "./ajax.js";

// Commodity
// 新增商品
export function addCommodity(obj) {
    addForm('Commodity', obj)
}
// 分页查询
export function getCommodity({name, page, size}) {
    return getFormPage('Commodity', {name}, page, size)
}
// 编辑商品
export function editCommodity(obj) {
    editForm(obj, 'Commodity')
}
// 删除商品
export function deleCommodity(id) {
    deleForm(id / 1, 'Commodity')
}

// 会员管理
// 新增会员
export function addUser(obj) {
    addForm('User', obj)
}
// 分页查询会员
export function getUser({query, page, size}) {
    return getFormPage('User', query, page, size)
}
export function getUserById(id) {
    let allValue = getForm('User')
    return allValue.find(i => i.id === id)
}
// 删除会员
export function deleUser(id) {
    deleForm(id / 1, 'User')
}
// 编辑会员
export function editUser(obj) {
    editForm(obj, 'User')
}


// 消费记录管理
// 新增记录
export function addRecord(obj) {
    addForm('Record', obj)
    // 修改user数据 ↓
    syncUser(obj.user)
    syncCommodity(obj.commodity)
}
function syncUser(id) {
    let allRecord = getForm('Record')
    allRecord = allRecord.filter(i => i.user === id)
    let paytotal = 0
    let integral = 0
    allRecord.forEach(i => {
        integral += i.integral / 1
        paytotal += i.net / 1
    })
    const user = getUserById(id)
    user.integral = integral
    user.paytotal = paytotal
    editUser(user)
}
function syncCommodity(clist) {
    if (clist && clist.length) {
        const allCommodity = Object.fromEntries(getForm('Commodity').map(item => [item.id, item]));
        clist.forEach(i => {
            allCommodity[i.id].surplus-=(i.number / 1)
        })
        setForm('Commodity', Object.values(allCommodity))
    }
}
// 删除记录
export function deleRecord(id) {
    deleForm(id / 1, 'Record')
}
// 编辑记录
export function editRecord(obj) {
    editForm(obj, 'Record')
    syncUser(obj.user)
    syncCommodity(obj.commodity)
}

// 分页查询记录管理
export function getRecord({query, page, size}) {
    let allValue = getForm('Record')
    const allUser = Object.fromEntries(getForm('User').map(item => [item.id, item]));
    // const allUser = getForm('User').reduce((accumulator, current) => {
    //     accumulator[current.id] = current;
    //     return accumulator;
    // }, {});
    allValue.forEach(i => {
        i.userName = allUser[i.user].name
        i.userPhone = allUser[i.user].phone
        i.userId = i.user
        i.commodityName = i.commodity.map(t => t.name).join(',')
    })
    const { name, phone, commodity } = query
    if (name) {
        allValue = allValue.filter(i => i.userName.includes(name))
    }
    if (phone) {
        allValue = allValue.filter(i => i.userPhone.includes(phone))
    }
    if (commodity) {
        allValue = allValue.filter(i => {
            let flag = false
            i.commodity.forEach(c => {
                if (c.name && c.name.includes(commodity)){
                    flag = true
                }
            })
            return flag
        })
    }
    return {
        total: allValue.length,
        list: allValue.splice((page - 1) * size, size)
    }
}

