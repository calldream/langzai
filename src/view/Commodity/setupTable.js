import { ref, reactive } from 'vue'
import { getCommodity, deleCommodity, editCommodity } from '../../api/api.js'
import { Message } from 'element3'

export const page = reactive({
    current: 1,
    total: 0
})

export const queryValue = reactive({
    name: ''
})

export const tableData = reactive([])

export function queryList() {
    page.current = 1
    getList()
}
export function getList() {
    const p = {
        ...queryValue,
        page: page.current,
        size: 10
    }
    const a = getCommodity(p)
    // console.log(a);
    tableData.length = 0 // 清空数组
    tableData.push(...a.list)
    page.total = a.total

}

export function dele({id}) {
    deleCommodity(id)
    Message({
        message: '删除成功！',
        type: 'success'
    })
    getList()
}

