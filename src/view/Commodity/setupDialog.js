import { ref } from 'vue'


// 工厂函数，解决堆栈问题
export function setupDialog(add, editTitle) {
    const dialogDom = ref(null)
    const dialogTitle = ref(add)
    function openDialog(t) {
        // console.log(dialogDom);
        // console.log(dialogDom.value);
        // 目前会在devtool 上边报 warn ，说是没有key，但是这个好像不是代码的问题
        // 好像是 devtool 本身的问题
        dialogTitle.value = add
        dialogDom.value.open()

    }

    function edit(obj) {
        dialogTitle.value = editTitle
        dialogDom.value.open(obj)
    }
    return {
        dialogDom,
        dialogTitle,
        openDialog,
        edit
    }
}
