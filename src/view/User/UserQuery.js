import { Message } from 'element3'
import { reactive } from 'vue'

export function querySetup(getAjax, deleAjax) {
    const queryValue = reactive({
        name: '',
        phone: '',
        commodity: ''
    })
    const page = reactive({
        current: 1,
        total: 0
    })
    const tableData = reactive([])

    function queryList() {
        page.current = 1
        getList()
    }

    function getList() {
        const p = {
            query: queryValue,
            page: page.current,
            size: 10
        }
        const a = getAjax(p)
        // console.log(a);
        tableData.length = 0 // 清空数组
        tableData.push(...a.list)
        // console.log(a);
        page.total = a.total
    }
    function dele(id) {
        console.log(id);
        deleAjax(id)
        Message({
            message: '删除成功！',
            type: 'success'
        })
        getList()
    }
    // 输出
    return {
        queryValue,
        page,
        tableData,
        queryList,
        dele,
        getList
    }
}
